ensembles="A30.32 A40.32  A60.24 A80.24 B25.32 B35.32   B55.32       B75.32  D15.48 D20.48   D30.48 "   #A40.40
#ensembles=" B25.32 B35.32   B55.32       B75.32  D15.48 D20.48   D30.48 "   #A40.40
#ensembles="  D15.48 D20.48   D30.48 "   #A40.40
#ensembles=" B25.32 B35.32   B55.32       B75.32   "   #A40.40
pdf=no

make

for e in $ensembles
do
#  ./reph read_plateaux -p ../../../realphoton_long_plateaux/$e jack $pdf
#  ./reph read_plateaux -p ../../../realphoton_up/$e jack $pdf
#  ./reph read_plateaux -p ../../../realphoton_somma_jr_prima_ZA_lp/$e jack $pdf
  ./reph read_plateaux -p ../../../realphoton_plateaux_lp/$e jack $pdf
  ./reph read_plateaux -p ../../../realphoton_plateaux/$e jack $pdf

#  ./reph read_plateaux -p ../../../prove/$e jack $pdf
#  ./reph read_plateaux -p ../../../realphoton_p0/$e jack $pdf
#  ./reph read_plateaux -p ../../../realphoton_symmetries/$e jack $pdf
# ppp=`head -n 1  ../../../realphoton/$e/plateaux_RA.txt`
 echo $e 
done
