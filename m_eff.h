#ifndef m_eff_H
#define m_eff_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <complex.h>
#include "linear_fit.h"
#include "mutils.h"
#include "jacknife.h"

double *constant_fit_m_eff(int M, double in){
    double *r;
    
    r=(double*) malloc(sizeof(double)*M);
    r[0]=1.;
    
    return r;
}
double M_eff_t( int t,int L0, double ***in, int num_corr){
    double mass;
 

    double ct[1],ctp[1],res,tmp_mass, u,d ;
    int i;
    ct[0]=in[num_corr][t][0];
    ctp[0]=in[num_corr][t+1][0];


    mass=log(ct[0]/ctp[0]);

    res=1;
    i=t;
    while(res>1e-19){
	     u=1.+exp(-mass*(L0-2*i-2));
	     d=1.+exp(-mass*(L0-2*i));
	     tmp_mass=log( (ct[0]/ctp[0]) * (u/d)) ;
	     res=fabs(tmp_mass - mass);
	     mass=tmp_mass;
    }
     return mass;

}


double *create_jmeff_from_jcorr(int Njack,int L0, int tmin ,int tmax, double ****in, int num_corr ){
   double **r,***y,**mt;
   double *fit,**tmp,*x;
   int i,j;
    
 
   fit=(double*) malloc(sizeof(double)*Njack);
   mt=(double**) malloc(sizeof(double*)*L0);
   r=(double**) malloc(sizeof(double*)*L0);
   x=(double*) malloc(sizeof(double)*(tmax-tmin));
   for(i=0;i<L0;i++)
        r[i]=(double*) malloc(sizeof(double)*Njack);
    
   y=(double***) malloc(sizeof(double**)*Njack);
   for (j=0;j<Njack;j++){
        y[j]=(double**) malloc(sizeof(double*)*(tmax-tmin));
        for (i=tmin;i<tmax;i++){
	       y[j][i-tmin]=(double*) malloc(sizeof(double)*2);
        }
   }

   for(i=1;i<L0/2;i++){    
        for (j=0;j<Njack;j++){
            r[i][j]=M_eff_t(i,L0,in[j],num_corr);
            if (i>=tmin && i<tmax){
                y[j][i-tmin][0]=r[i][j];
            }

        }
        mt[i]=mean_and_error_jack(Njack, r[i]);
        
        if (i>=tmin && i<tmax){
            for (j=0;j<Njack;j++){
                y[j][i-tmin][1]=mt[i][1];
            }
        }
    free(mt[i]);
    }
    free(mt);

    for (j=0;j<Njack;j++){
        tmp=linear_fit( tmax-tmin, x, y[j],  1,constant_fit_m_eff );
        fit[j]=tmp[0][0];
        free(tmp[0]);free(tmp);
    }
    

   for(i=0;i<L0;i++)
      free(r[i]);
   free(r);

   for (j=0;j<Njack;j++){
        for (i=tmin;i<tmax;i++){
		free(y[j][i-tmin]);
        }
        free(y[j]);
   }
   free(y);
   free(x);
   return fit;
}

double ratio_i( int t, double ***in){
    double mass;
    complex double a,b,c,d;
    int i;    
    mass=0;
 
      mass=( (in[4][t][1]+in[8][t][1]+in[12][t][1])/(3.*in[0][t][0]) );// *sqrt(in[0][t][0]*in[1][t][0]/(in[2][t][0]*in[3][t][0]));
    return mass;
}
double ratio_0( int t, double ***in){
    double mass;
    complex double a,b,c,d;    
     
    a=in[0][t][0]+I*in[0][t][1];
    b=in[1][t][0]+I*in[1][t][1];
    c=in[2][t][0]+I*in[2][t][1];
    d=in[3][t][0]+I*in[3][t][1];
    
    mass=creal( a*b/(c*d) ) ;
    mass=in[0][t][0]*in[1][t][0]/(in[2][t][0]*in[3][t][0]);
   // mass=log(in[0][t][0]/in[0][t+1][0]);
   //mass=( (in[4][t][1]+in[8][t][1]+in[12][t][1])/(in[0][t][0]) );
    return mass;
}
/*
double **create_jratio_from_jcorr(int Njack,int L0, int tmin ,int tmax, double ****in ){
   double **r,***y,**mt;
   double *fit,**tmp,*x;
   int i,j;
   double **V;
   
   V[0]=(double*) malloc(sizeof(double)*Njack);
   V[1]=(double*) malloc(sizeof(double)*Njack);
 
   mt=(double**) malloc(sizeof(double*)*L0);
   r=(double**) malloc(sizeof(double*)*L0);
   x=(double*) malloc(sizeof(double)*(tmax-tmin));
   for(i=0;i<L0;i++)
        r[i]=(double*) malloc(sizeof(double)*Njack);
    
   y=(double***) malloc(sizeof(double**)*Njack);
   for (j=0;j<Njack;j++){
        y[j]=(double**) malloc(sizeof(double*)*(tmax-tmin));
        for (i=tmin;i<tmax;i++){
	       y[j][i-tmin]=(double*) malloc(sizeof(double)*2);
        }
   }

   for(i=1;i<L0/2;i++){    
        for (j=0;j<Njack;j++){
            r[i][j]=ratio_0(i,L0,in[j]);
            if (i>=tmin && i<tmax){
                y[j][i-tmin][0]=r[i][j];
            }

        }
        mt[i]=mean_and_error_jack(Njack, r[i]);
        
        if (i>=tmin && i<tmax){
            for (j=0;j<Njack;j++){
                y[j][i-tmin][1]=mt[i][1];
            }
        }
    free(mt[i]);
    }

    for (j=0;j<Njack;j++){
        tmp=linear_fit( tmax-tmin, x, y[j],  1,constant_fit_m_eff );
        V[0][j]=tmp[0][0];
        free(tmp[0]);free(tmp);
    }
   

   for(i=1;i<L0/2;i++){    
        for (j=0;j<Njack;j++){
            r[i][j]=ratio_i(i,L0,in[j]);
            if (i>=tmin && i<tmax){
                y[j][i-tmin][0]=r[i][j];
            }

        }
        mt[i]=mean_and_error_jack(Njack, r[i]);
        
        if (i>=tmin && i<tmax){
            for (j=0;j<Njack;j++){
                y[j][i-tmin][1]=mt[i][1];
            }
        }
    free(mt[i]);
    }

    for (j=0;j<Njack;j++){
        tmp=linear_fit( tmax-tmin, x, y[j],  1,constant_fit_m_eff );
        V[1][j]=tmp[0][0];
        free(tmp[0]);free(tmp);
    }
    

   free(mt);
   for(i=0;i<L0;i++)
      free(r[i]);
   free(r);

   for (j=0;j<Njack;j++){
        for (i=tmin;i<tmax;i++){
		free(y[j][i-tmin]);
        }
        free(y[j]);
   }
   free(y);
   free(x);
   return fit;
}
*/
#endif
