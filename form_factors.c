#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <complex.h>


#include "jacknife.h"
#include "read.h"
#include "m_eff.h"
#include "gnuplot.h"

 
static struct
{
    int twist;
    int nf;
    int nsrc,nsrcd,*y0;
    int l0,l1,l2,l3;
    int nk,nmoms;
    double beta,ksea,musea,csw;
    double *k,*mu,(*mom)[4];
} file_head;



static void  print_file_head(FILE *stream)
{
    int i,dsize;
    double *dstd;
    
    fprintf(stream,"%d\n",file_head.twist);
    fprintf(stream,"%d\n",file_head.nf);
    fprintf(stream,"%d\n",file_head.nsrc);
    fprintf(stream,"%d\n",file_head.l0);
    fprintf(stream,"%d\n",file_head.l1);
    fprintf(stream,"%d\n",file_head.l2);
    fprintf(stream,"%d\n",file_head.l3);
    fprintf(stream,"%d\n",file_head.nk);
    fprintf(stream,"%d\n",file_head.nmoms);
    
    fprintf(stream,"%f\n",file_head.beta);
    fprintf(stream,"%f\n",file_head.ksea);
    fprintf(stream,"%f\n",file_head.musea);
    fprintf(stream,"%f\n",file_head.csw);
   
    for(i=0;i<2*file_head.nk;++i)
            fprintf(stream,"%f\n",file_head.k[i]);

    
    for(i=0;i<file_head.nmoms;++i)
           fprintf(stream,"%f  %f   %f   %f\n",file_head.mom[i][0],file_head.mom[i][1],file_head.mom[i][2],file_head.mom[i][3]);
}

static void  read_file_head_bin(FILE *stream)
{
    int i,dsize;
    double *dstd;
    
    fread(&file_head.twist,sizeof(int),1,stream);
    fread(&file_head.nf,sizeof(int),1,stream);
    fread(&file_head.nsrc,sizeof(int),1,stream);
    fread(&file_head.l0,sizeof(int),1,stream);
    fread(&file_head.l1,sizeof(int),1,stream);
    fread(&file_head.l2,sizeof(int),1,stream);
    fread(&file_head.l3,sizeof(int),1,stream);
    fread(&file_head.nk,sizeof(int),1,stream);
    fread(&file_head.nmoms,sizeof(int),1,stream);
    
    fread(&file_head.beta,sizeof(double),1,stream);
    fread(&file_head.ksea,sizeof(double),1,stream);
    fread(&file_head.musea,sizeof(double),1,stream);
    fread(&file_head.csw,sizeof(double),1,stream);
   
    file_head.k=(double*) malloc(sizeof(double)*2*file_head.nk);
    for(i=0;i<2*file_head.nk;++i)
    	fread(&file_head.k[i],sizeof(double),1,stream);
    
    file_head.mom=malloc(sizeof(file_head.mom[4])*file_head.nmoms);
    for(i=0;i<file_head.nmoms;i++) {
        fread(&file_head.mom[i][0],sizeof(double),1,stream);
        fread(&file_head.mom[i][1],sizeof(double),1,stream);
        fread(&file_head.mom[i][2],sizeof(double),1,stream);
        fread(&file_head.mom[i][3],sizeof(double),1,stream);

    }
}


void read_nconfs(int *s, int *c, FILE *stream){

   FILE *f1;
   long int tmp;

   fread(s,sizeof(int),1,stream);
   f1=stream;
   
   fseek(stream, 0, SEEK_END);
   tmp = ftell(stream);
   tmp-= sizeof(double)* (file_head.nmoms*4 + file_head.nk*2+4 )+ sizeof(int)*9 ;
   tmp-= sizeof(int);
   (*c)= tmp/ (sizeof(int)+ (*s)*sizeof(double) );
 
  rewind(stream);
  read_file_head_bin(stream);
  fread(s,sizeof(int),1,stream);

}


double *constant_fit(int M, double in){
    double *r;
    
    r=(double*) malloc(sizeof(double)*M);
    r[0]=1.;
    
    return r;
}
double M_eff( int t, double ***in){
    double mass;
 
    double ct[1],ctp[1],res,tmp_mass, u,d ;
    int i,L0;
    L0=file_head.l0;
    ct[0]=in[0][t][0];
    ctp[0]=in[0][t+1][0];


    mass=log(ct[0]/ctp[0]);

    res=1;
       i=t;
          while(res>1e-19){
                     u=1.+exp(-mass*(L0-2*i-2));
                     d=1.+exp(-mass*(L0-2*i));
                     tmp_mass=log( (ct[0]/ctp[0]) * (u/d)) ;
                     res=tmp_mass - mass;
                     mass=tmp_mass;
          }
     return mass;

}


static int index_twopt(int si,int ii,int ix0,int imom1,int imom2,int ik1,int ik2)
{
    int nk,nmoms;

    nk=file_head.nk;
    nmoms=file_head.nmoms;

    return ii+si*(ix0+file_head.l0*(imom1+nmoms*(imom2+nmoms*(ik1+nk*ik2))));
}
static int index_threept(int si,int ii,int ix0,int imom1,int imom2,int ik1,int ik2,int ik3)
{
    int nk,nmoms;

    nk=file_head.nk;
    nmoms=file_head.nmoms;

    return ii+si*(ix0+file_head.l0*(imom1+nmoms*(imom2+nmoms*(ik1+nk*(ik2+nk*ik3)))));
}
int index_minus_kappa(int ik)
{
    int imk,i;
    double mu;
    
    mu=-file_head.k[ file_head.nk+ik ];
    imk=-1;
    for (i=0;i<file_head.nk;i++){
	if ( file_head.k[ file_head.nk+i ]==mu )
            imk=i;
    }

   error(imk==-1,1,"inde_minus_kappa ",  "Unable to find mass=%g",mu);
   return imk; 


}

static int index_minus_theta(int imom)
{
   int i,imth;
   double m0,m1,m2,m3;

   imth=-1;
   m0= file_head.mom[imom][0];
   m1= -file_head.mom[imom][1];
   m2= -file_head.mom[imom][2];
   m3= -file_head.mom[imom][3];
   for(i=0;i<file_head.nmoms;++i)
      if(m0==file_head.mom[i][0] && m1==file_head.mom[i][1] && 
	 m2==file_head.mom[i][2] && m3==file_head.mom[i][3])
	 imth=i;

   error(imth==-1,1,"inde_minus_theta ",  "Unable to find theta=%g",m1);
   return imth;
}


void extract_twopt(double *to_read , double **to_write,int si, int ii, int imom1, int imom2, int ik1, int ik2 ){

   int mik1, mik2;
   int mimom1,mimom2;
   int t,vol,index;
   double re,im;

  
   mik1=index_minus_kappa(ik1);
   mik2=index_minus_kappa(ik2);
   mimom1=index_minus_theta(imom1);
   mimom2=index_minus_theta(imom2);

	for(t=0;t<file_head.l0;t++){
	   re=0;vol=0;
	   index=2*index_twopt(si,ii,t,imom1,imom2,ik1,ik2);
	   re+= to_read[index];
    	   im+= to_read[index+1];
      	   vol++;
/*     	   index=2*index_twopt(si,ii,t,imom2,imom1,ik1,ik2);
	   re+= to_read[index];
       	   im+= to_read[index+1];
	   vol++;
	   index=2*index_twopt(si,ii,t,imom1,imom2,mik1,mik2);
	   re+= to_read[index];
       	   im+= to_read[index+1];
           vol++;
	   index=2*index_twopt(si,ii,t,imom2,imom1,mik1,mik2);
	   re+= to_read[index];
           im+= to_read[index+1];
           vol++;
	   index=2*index_twopt(si,ii,t,imom1,imom2,ik2,ik1);
	   re+= to_read[index];
      	   im+= to_read[index+1];
           vol++;
	   index=2*index_twopt(si,ii,t,imom1,imom2,mik2,mik1);
	   re+= to_read[index];
           im+= to_read[index+1];
           vol++;*/
	  /* if (  mimom1>=0 &&  mimom2 >=0 )
	   { 
		   index=2*index_twopt(si,ii,t,mimom1,mimom2,ik1,ik2);
		   re+= to_read[index];
                   im+= to_read[index+1];
                   vol++;
		   index=2*index_twopt(si,ii,t,mimom2,mimom1,ik1,ik2);
		   re+= to_read[index];
	           im+= to_read[index+1];
        	   vol++;
		   index=2*index_twopt(si,ii,t,mimom1,mimom2,ik2,ik1);
		   re+= to_read[index];
	           im+= to_read[index+1];
        	   vol++;
		   index=2*index_twopt(si,ii,t,mimom2,mimom1,mik2,mik1);
		   re+= to_read[index];
	           im+= to_read[index+1];
        	   vol++;
	   }*/ 
	   to_write[t][0]=re/( (double) vol );
	   to_write[t][1]=im/( (double) vol );
	}
}
 
void extract_threept(double *to_read , double **to_write,int si, int ii, int imom1, int imom2, int ik1, int ik2 ,int ik3,int sym ){

   int mik1, mik2,mik3;
   int mimom1,mimom2;
   int t,vol,index;
   double re,im;

   double symm=(double) sym;
   mik1=index_minus_kappa(ik1);
   mik2=index_minus_kappa(ik2);
   mik3=index_minus_kappa(ik3);
   mimom1=index_minus_theta(imom1);
   mimom2=index_minus_theta(imom2);

	for(t=0;t<file_head.l0;t++){
	   re=0;vol=0;im=0;
	   index=2*index_threept(si,ii,t,imom1,imom2,ik1,ik2,ik3);
	   re+= to_read[index];
	   im+= to_read[index+1];
           vol++;
	   index=2*index_threept(si,ii,t,imom1,imom2,mik1,mik2,mik3);
	   re+= to_read[index];
	   im+= to_read[index+1];
           vol++;
           if (  ik1 ==ik2){
		   index=2*index_threept(si,ii,t,imom1,imom2,mik1,ik1,mik3);
		   re+= to_read[index];
	           im+= to_read[index+1];
		   vol++;
		   index=2*index_threept(si,ii,t,imom1,imom2,ik3,mik3,ik1);
		   re+= to_read[index];
                   im+= to_read[index+1];
                   vol++;
           }
  

	   if (  mimom1>=0 &&  mimom2 >=0 )
	   { 
		   index=2*index_threept(si,ii,t,mimom1,mimom2,ik1,ik2,ik3);
		   re+=symm* to_read[index];
	           im+=symm* to_read[index+1];
                   vol++;
		   index=2*index_threept(si,ii,t,mimom1,mimom2,mik1,mik2,mik3);
		   re+=symm* to_read[index];
	           im+=symm* to_read[index+1];
		   vol++;
           if (  ik1 ==ik2){
		   index=2*index_threept(si,ii,t,mimom1,mimom2,mik3,ik3,mik1);
		   re+= to_read[index];
	           im+= to_read[index+1];
		   vol++;
		   index=2*index_threept(si,ii,t,mimom1,mimom2,ik3,mik3,ik1);
		   re+= to_read[index];
                   im+= to_read[index+1];
                   vol++;
           }
	   }
	   to_write[t][0]=re/( (double) vol );
	   to_write[t][1]=im/((double) vol);
	}
}
void main(int argc, char **argv){
   int size;
   int i,j,t;
   FILE  *f=NULL;
   int *iconf,confs;
   double ****data, **out,**tmp; 
   char c;
   clock_t t1,t2;
   double *in;

   
   double *fit,***y,*x,*m,*me;
   FILE *outfile=NULL;

   double ****conf_jack,**r,**mt,**met;
   int Ncorr=1;
   outfile=fopen("out_E0","w+");
   error(outfile==NULL,1,"main ",
         "Unable to open output file");


   error(argc!=2,1,"main ",
         "usage: ./form_factors blind/see");

   
   
   double E_B,E_Pi, x_SCHET,q2,vec_pB,vec_pPi;
   int Neff,Njack;
   t1=clock();

   f=fopen("./oPPo-ss_bin10.dat","r");
   if (f==NULL) {printf("2pt file not found\n"); exit(0);}

     
   read_file_head_bin(f);
   print_file_head(outfile);
   fflush(outfile);

   printf("size=%d  confs=%d\n",size,confs);
   read_nconfs(&size,&confs,f);   
////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
//confs=287;
//printf("number of confs set manually to %d\n",confs);
////////////////////////////////////////////////////////////////////
   iconf=(int*) malloc(sizeof(int)*confs);
   out=(double**) malloc(sizeof(double*)*confs);
   
   
   
   printf("reading confs:\n");
   for(i=0;i<confs;i++){
       fread(&iconf[i],sizeof(int),1,f);
       out[i]=(double*) malloc(sizeof(double)*size);
       fread(out[i],sizeof(double),size,f);
       printf("%d\t",iconf[i]);
   }
   printf("\n");
   
   fclose(f);
   //copyng the input
   int var=1,si_2pt,si;
   data=(double****) malloc(sizeof(double***)*confs);
   for (i=0;i<confs;i++){
        data[i]=(double***) malloc(sizeof(double**)*var);
        for (j=0;j<var;j++){
            data[i][j]=(double**) malloc(sizeof(double*)*file_head.l0);
            for(t=0;t<file_head.l0;t++)
                data[i][j][t]=(double*) malloc(2*sizeof(double));
        }
   }
   
   int ii, imom1, imom2, ik1,ik2,ik3;
   ii=0;
   imom1=0;
   imom2=0;
   ik1=0;
   ik2=0;
   ik3=0;
   int tmin=20, tmax=27, sep=1;
   int yn;
  
   int index;
   double **E0,***Mass;
   E0=(double**) malloc(sizeof(double*)*size);
   for (i=0;i<size ;i++)
	E0[i]=(double*) malloc(sizeof(double)*2); 

   Neff=confs;
   Njack=Neff+1;
   Mass=(double***) malloc(sizeof(double**)*file_head.nk);
   for(ik2=0;ik2<file_head.nk;ik2++){
   	Mass[ik2]=(double**) malloc(sizeof(double*)*file_head.nk);
   	for(ik1=0;ik1<file_head.nk;ik1++){
   		Mass[ik2][ik1]=(double*) malloc(sizeof(double)*Njack);
        }
   }

   double ave=0;
   int vol=0;
   si_2pt=size/(file_head.nk*file_head.nk*file_head.nk*file_head.nmoms*file_head.nmoms*file_head.l0*2);
   if (si_2pt==0)
       si_2pt=size/(file_head.nk*file_head.nk*file_head.nmoms*file_head.nmoms*file_head.l0*2);
   
   
   
   int mik3, mik2,mik1, mimom1,mimom2,counter;
   
   
   for(ik2=0;ik2<file_head.nk;ik2++){
   for(ik1=0;ik1<file_head.nk;ik1++){
   for(imom2=0;imom2<file_head.nmoms;imom2++){
   for(imom1=0;imom1<file_head.nmoms;imom1++){

       mik2=index_minus_kappa(ik2);
       mik1=index_minus_kappa(ik1);
       mimom1=index_minus_theta(imom1);
       mimom2=index_minus_theta(imom2);
   for (i=0;i<confs;i++){
       extract_twopt(out[i] ,data[i][0],si_2pt,ii,imom1,imom2,ik1,ik2 );
   }
   
   symmetrise_corr(Neff, 0, file_head.l0,data);
   conf_jack=create_jack(Neff, 1, file_head.l0, data);
  

////////////////////allocation
   r=(double**) malloc(sizeof(double*)*file_head.l0);
   for(i=0;i<file_head.l0;i++)
        r[i]=(double*) malloc(sizeof(double)*Njack);
    
   mt=(double**) malloc(sizeof(double*)*file_head.l0);
   fit=(double*) malloc(sizeof(double)*Njack);
   y=(double***) malloc(sizeof(double**)*Njack);
   for (j=0;j<Njack;j++){
        y[j]=(double**) malloc(sizeof(double*)*(tmax-tmin));
        for (i=tmin;i<tmax;i++){
	       y[j][i-tmin]=(double*) malloc(sizeof(double)*2);
        }
    }
    x=(double*) malloc(sizeof(double)*(tmax-tmin));
////////////////end 

/////////////////M_PS
   fprintf(outfile,"#m_eff(t) from  propagators:1) mu %g theta %g 2) mu %g  theta %g\n",
	file_head.k[mik1+file_head.nk],file_head.mom[mimom1][1],
	file_head.k[ik2+file_head.nk], file_head.mom[imom2][1] );
   
   for(i=1;i<file_head.l0/2;i++){    
        for (j=0;j<Njack;j++){
            r[i][j]= M_eff(i,conf_jack[j]);
            if (i>=tmin && i<tmax){
                y[j][i-tmin][0]=r[i][j];
            }

        }
        mt[i]=mean_and_error_jack(Njack, r[i]);
        
        if (i>=tmin && i<tmax){
            for (j=0;j<Njack;j++){
                y[j][i-tmin][1]=mt[i][1];
            }
        }
    fprintf(outfile,"%d   %.15e    %.15e\n",i,mt[i][0],mt[i][1]);
    free(mt[i]);
    }

    for (j=0;j<Njack;j++){
        tmp=linear_fit( tmax-tmin, x, y[j],  1,constant_fit );
        fit[j]=tmp[0][0];
        if( imom1==0 && imom2==0 )
	        Mass[ik1][ik2][j]=fit[j];   
        free(tmp[0]);free(tmp);
    }

    m=mean_and_error_jack(Njack, fit);
    fprintf(outfile,"\n\n #M_PS fit in [%d,%d]\n  %.15g    %.15g\n\n\n",tmin,tmax-1,m[0],m[1]);

    fflush(outfile);
/////////////////////free memory
    for(i=0;i<file_head.l0;i++)
   	  free(r[i]);
    free(r); free(fit);
    free(m);free(mt);
    for (j=0;j<Njack;j++){
        for (i=tmin;i<tmax;i++){
	       free(y[j][i-tmin]);
        }
        free(y[j]);
    }
    free(y);
    free(x);
//////////////////////////end
   }}}} // end loop imom1 imom2 ik1 ik2
   fclose(outfile);
   for (i=0;i<confs;i++){
        for (j=0;j<var;j++){
            for(t=0;t<file_head.l0;t++)
               free(data[i][j][t]);
            free(data[i][j]);
        }
        free(data[i]);
   }
   for (i=0;i<confs;i++)
       free(out[i]);
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   //3pt
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   tmin=12; tmax=16;

   FILE *outfile_i;   
   printf("\nstarting 3pt\n");
   outfile=fopen("out_ME_V0","w+");
   error(outfile==NULL,1,"main ",
         "Unable to open output file");
   outfile_i=fopen("out_ME_Vi","w+");
   error(outfile==NULL,1,"main ",
         "Unable to open output file");

   f=fopen("./oPVmuPo-sss_bin10.dat","r");
   error(outfile==NULL,1,"main ",
         "Unable to open 3pt data file");

   read_file_head_bin(f);
   print_file_head(outfile);
   fflush(outfile);

   read_nconfs(&size,&confs,f);   


////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
//confs=287;
//printf("number of confs set manually to %d\n",confs);
////////////////////////////////////////////////////////////////////

   printf("reading confs:\n");


   for(i=0;i<confs;i++){
       fread(&iconf[i],sizeof(int),1,f);
       out[i]=(double*) malloc(sizeof(double)*size);
       fread(out[i],sizeof(double),size,f);
       printf("%d\t",iconf[i]);
       fflush(stdout);
   }
   printf("\n");
   fclose(f);

   si=size/(file_head.nk*file_head.nk*file_head.nk*file_head.nmoms*file_head.nmoms*file_head.l0*2);
   if (si==0)
       si=size/(file_head.nk*file_head.nk*file_head.nmoms*file_head.nmoms*file_head.l0*2);

   var=4*si;

   data=(double****) malloc(sizeof(double***)*confs);
   for (i=0;i<confs;i++){
        data[i]=(double***) malloc(sizeof(double**)*var);
        for (j=0;j<var;j++){
            data[i][j]=(double**) malloc(sizeof(double*)*file_head.l0);
            for(t=0;t<file_head.l0;t++)
                data[i][j][t]=(double*) malloc(2*sizeof(double));
        }
   }

   

   int sym;
   int m1,m2,k1,k2,k3;
   for(ik3=0/*file_head.nk-1*/;ik3<file_head.nk;++ik3){
   for(ik2=0/*file_head.nk-1*/;ik2<file_head.nk;++ik2){
   for(ik1=0;ik1<file_head.nk;++ik1){
   for(imom2=0;imom2<file_head.nmoms;imom2++){
   for(imom1=0;imom1<file_head.nmoms;imom1++){


   for (i=0;i<confs;i++){
  
       mik3=index_minus_kappa(ik3);
       mik2=index_minus_kappa(ik2);
       mik1=index_minus_kappa(ik1);
       mimom1=index_minus_theta(imom1);
       mimom2=index_minus_theta(imom2);

       counter=0;
       for (ii=0;ii<si;ii++){
               if (ii >0 ) sym=-1;
	       if (ii ==0) sym=1;
	       extract_threept(out[i],data[i][counter+0],si,ii,imom1,imom2,ik1,ik2,ik3,sym );
	       extract_threept(out[i],data[i][counter+1],si,ii,imom2,imom1,mik2,mik1,ik3,sym );
	       extract_threept(out[i],data[i][counter+2],si,ii,imom1,imom1,ik1,mik1,ik3 ,sym);
	       extract_threept(out[i],data[i][counter+3],si,ii,imom2,imom2,mik2,ik2,ik3 ,sym);
               counter+=4;
       }
      
      
   }


   Neff=confs;
 //  symmetrise_corr(Neff, var-1, file_head.l0,data);
 //  symmetrise_corr(Neff, var-2, file_head.l0,data);
/*   antisymmetrise_corr(Neff, 0, file_head.l0,data);
   antisymmetrise_corr(Neff, 1, file_head.l0,data);
   antisymmetrise_corr(Neff, 2, file_head.l0,data);
   antisymmetrise_corr(Neff, 3, file_head.l0,data);
 
   symmetrise_corr(Neff, 4, file_head.l0,data);
   symmetrise_corr(Neff, 8, file_head.l0,data);
   symmetrise_corr(Neff, 12, file_head.l0,data);
  */ 
   conf_jack=create_jack(Neff, var, file_head.l0, data);
    
   Njack=Neff+1;

   double *M_B,*M_Pi,*JM_B,*JM_Pi;
   JM_B=(double*) malloc(sizeof(double)*Njack);
   JM_Pi=(double*) malloc(sizeof(double)*Njack);
   for (j=0;j<Njack;j++){
	JM_B[j]=Mass[ik1][ik3][j];
	JM_Pi[j]=Mass[mik2][ik3][j];
   }

   M_B=mean_and_error_jack(Njack, JM_B);
   M_Pi=mean_and_error_jack(Njack,JM_Pi);

   fprintf(outfile,"#M_B=%g -+ %g\t ",M_B[0],M_B[1]);
   fprintf(outfile,"#M_Pi=%g +- %g\n ",M_Pi[0],M_Pi[1]);
////////////////////allocation
   r=(double**) malloc(sizeof(double*)*file_head.l0);
   for(i=0;i<file_head.l0;i++)
        r[i]=(double*) malloc(sizeof(double)*Njack);
    
   met=(double**) malloc(sizeof(double*)*file_head.l0);
   fit=(double*) malloc(sizeof(double)*Njack);
   y=(double***) malloc(sizeof(double**)*Njack);
  
////////////////end 

///////////////////////kynematic
   

   
   fprintf(outfile,"#matrix elements(t) from  propagators physical:\n");
   fprintf(outfile,"#1) mu %g theta %g 2) mu %g theta %g 3) mu %g theta %g\n",
	file_head.k[mik1+file_head.nk],file_head.mom[mimom1][1],
	file_head.k[ik2+file_head.nk],file_head.mom[mimom2][1],
	file_head.k[ik3+file_head.nk],file_head.mom[mimom2][1] );   
   vec_pB=sqrt(3)*2*3.14159265358979 *(file_head.mom[mimom2][1]+ file_head.mom[mimom1][1])/file_head.l1 ;
   vec_pPi=sqrt(3)*2*3.14159265358979 *(2*file_head.mom[mimom2][1])/file_head.l1 ;
   E_B=sqrt(M_B[0]*M_B[0]+ vec_pB*vec_pB);
   E_Pi=sqrt(M_Pi[0]*M_Pi[0]+ vec_pPi*vec_pPi);

   fprintf(outfile,"#E_B=%g\t ",E_B);
   fprintf(outfile,"#E_Pi=%g\n ",E_Pi);

   fprintf(outfile,"#p_B=%g p_Pi=%g\n",vec_pB,vec_pPi);
   x_SCHET=2*(   E_B*E_Pi- vec_pB*vec_pPi )/ ( (E_B*E_B- vec_pB*vec_pB) );
   q2=(E_B-E_Pi)*(E_B-E_Pi)  -   (vec_pB-vec_pPi)*(vec_pB-vec_pPi);
   fprintf(outfile,"#x_SCHET=%g q^2=%g\n",x_SCHET,q2);
   
   fprintf(outfile_i,"#matrix elements(t) from  propagators physical:\n");
   fprintf(outfile_i,"#1) mu %g theta %g 2) mu %g theta %g 3) mu %g theta %g\n",
	file_head.k[mik1+file_head.nk],file_head.mom[mimom1][1],
	file_head.k[ik2+file_head.nk],file_head.mom[mimom2][1],
	file_head.k[ik3+file_head.nk],file_head.mom[mimom2][1] );   
   fprintf(outfile_i,"#E_B=%g \t",E_B);
   fprintf(outfile_i,"#E_Pi=%g \n",E_Pi);
   fprintf(outfile_i,"#p_B=%g p_Pi=%g\n",vec_pB,vec_pPi);
   fprintf(outfile_i,"#x_SCHET=%g q^2=%g\n",x_SCHET,q2);
   
/////////////////matrix element
   for(i=0;i<file_head.l0-1;i++){
        for (j=0;j<Njack;j++){
            r[i][j]=ratio_0(i,conf_jack[j]);
            /*r[i][j]=r[i][j]*4.*sqrt(JM_B[j]*JM_B[j]+ vec_pB*vec_pB)*sqrt(M_Pi[j]*M_Pi[j]+ vec_pPi*vec_pPi);*/
    //        r[i][j]=sqrt(r[i][j]);
            

        }
        met[i]=mean_and_error_jack(Njack, r[i]);
        
        fprintf(outfile,"%d   %.15g    %.15g\n",i,met[i][0],met[i][1]);
   
   }
   yn=1;
   if ( strcmp(argv[1],"see")==0 ){
       while(yn>0){
            printf("#1) mu %g theta %g 2) mu %g theta %g 3) mu %g theta %g\n",
                file_head.k[mik1+file_head.nk],file_head.mom[mimom1][1],
                file_head.k[ik2+file_head.nk],file_head.mom[mimom2][1],
                file_head.k[ik3+file_head.nk],file_head.mom[mimom2][1] );   
            plotting( file_head.l0, met , &tmin,&tmax, &sep);
            me=try_linear_fit( tmin,  tmax,sep , met, r, Njack );
            yn=plotting_fit( file_head.l0, met , tmin,tmax,me);
            free(me);
       }
   }
   me=try_linear_fit( tmin,  tmax,sep , met, r, Njack );
   fprintf(outfile,"\n\n #matrix elemnt V0 fit in [%d,%d]\n  %.15g    %.15g\n\n\n",tmin,tmax,me[0],me[1]);

   for(i=0;i<file_head.l0-1;i++)
        free(met[i]);
   free(me);
   fflush(outfile);

//////////////////////////////////////////////////////
/////////////////matrix element   i
   for(i=0;i<file_head.l0-1;i++){
        for (j=0;j<Njack;j++){
            r[i][j]=ratio_i(i,conf_jack[j]);
            /*r[i][j]=r[i][j]*4.*sqrt(JM_B[j]*JM_B[j]+ vec_pB*vec_pB)*sqrt(M_Pi[j]*M_Pi[j]+ vec_pPi*vec_pPi);*/
    //        r[i][j]=sqrt(r[i][j]);
            

        }
        met[i]=mean_and_error_jack(Njack, r[i]);
        
        fprintf(outfile_i,"%d   %.15g    %.15g\n",i,met[i][0],met[i][1]);
   
   }
   yn=1;
   if ( strcmp(argv[1],"see")==0 ){
       while(yn>0){
            printf("#1) mu %g theta %g 2) mu %g theta %g 3) mu %g theta %g\n",
                file_head.k[mik1+file_head.nk],file_head.mom[mimom1][1],
                file_head.k[ik2+file_head.nk],file_head.mom[mimom2][1],
                file_head.k[ik3+file_head.nk],file_head.mom[mimom2][1] );   
           
            plotting( file_head.l0, met , &tmin,&tmax, &sep);
            me=try_linear_fit( tmin,  tmax,sep , met, r, Njack );
            yn=plotting_fit( file_head.l0, met , tmin,tmax,me);
            free(me);
       }
   }
   
   me=try_linear_fit( tmin,  tmax,sep , met, r, Njack );
   fprintf(outfile_i,"\n\n #matrix elemnt Vi fit in [%d,%d]\n  %.15g    %.15g\n\n\n",tmin,tmax,me[0],me[1]);

   for(i=0;i<file_head.l0-1;i++)
        free(met[i]);
   free(me);
   fflush(outfile);
/////////////////////free memory
    for(i=0;i<file_head.l0;i++)
   	  free(r[i]);
    free(r); free(fit);
    free(met);
    
    free(y);
    
    free(M_B); free(M_Pi);
///////////////////////////////////////////////////////end


   }}}}}///end loop imom1 imom2 ik1 ik2 ik3

   
   
   fclose(outfile);   fclose(outfile_i);
   for (i=0;i<confs;i++){
        for (j=0;j<var;j++){
            for(t=0;t<file_head.l0;t++)
               free(data[i][j][t]);
            free(data[i][j]);
        }
        free(data[i]);
   }
   for (i=0;i<confs;i++)
       free(out[i]);

  

 
   
}
