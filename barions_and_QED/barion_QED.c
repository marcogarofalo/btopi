#define CONTROL

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <complex.h>

#include <unistd.h>
 #include <sys/time.h>
 #include <fcntl.h>
 
 
#include "global.h"

#include "resampling.h"

#include "read.h"
#include "m_eff1.h"
#include "gnuplot.h"
#include "eigensystem.h"
#include "linear_fit.h"
#include "various_fits.h"
#include "indices.h"
//#include "continuum_reph.h"
#include "global_barion_QED.h"


#include "tower.h"
#include <unistd.h>
#include <omp.h>

int head_allocated=0;

int Nboot=100;
int fdA,fdV;
struct  kinematic kinematic_2pt;
struct  kinematic_G kinematic_2pt_G;

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
int r_value(int r)
{
    int vr;
    if (r==0) vr=1;
    else if (r==1) vr=-1;
    else error(0==0,1,"r_value nor 0 neither 1","");
    return vr;
}
void get_kinematic( int ik2, int ik1,int imom2, int imom1 ){
    kinematic_2pt.ik2=ik2;
    kinematic_2pt.ik1=ik1;

    kinematic_2pt.k2=file_head.k[ik2+file_head.nk];
    kinematic_2pt.k1=file_head.k[ik1+file_head.nk];
    
    kinematic_2pt.mom2=-file_head.mom[imom2][3];
    if (kinematic_2pt.mom2==0) kinematic_2pt.mom2=0;
    kinematic_2pt.mom1=file_head.mom[imom1][3];

    kinematic_2pt.mom02=file_head.mom[imom2][0];
    kinematic_2pt.mom01=file_head.mom[imom1][0];
    
    kinematic_2pt.r2=-1;
    kinematic_2pt.r1=1;
    
    
    int i ;
    kinematic_2pt.Mom2[0]=file_head.mom[imom2][0];
    kinematic_2pt.Mom1[0]=file_head.mom[imom1][0];
 
    for (i=1;i<4;i++){
        kinematic_2pt.Mom2[i]=-file_head.mom[imom2][i];
        if (kinematic_2pt.Mom2[i]==0) kinematic_2pt.Mom2[i]=0;
        kinematic_2pt.Mom1[i]=file_head.mom[imom1][i];
 
    }
        
 
}


double timestamp()
{
	struct timeval tm;
	gettimeofday(&tm, NULL);
	return tm.tv_sec + 1.0e-6 * tm.tv_usec;
}


static int ****mass_index;


static void  print_file_head(FILE *stream)
{
    int i;
    
    fprintf(stream,"twist= %d\n",file_head.twist);
    fprintf(stream,"nf=%d\n",file_head.nf);
    fprintf(stream,"nsrc=%d\n",file_head.nsrc);
    fprintf(stream,"L0=%d\n",file_head.l0);
    fprintf(stream,"L1=%d\n",file_head.l1);
    fprintf(stream,"L2=%d\n",file_head.l2);
    fprintf(stream,"L3=%d\n",file_head.l3);
    fprintf(stream,"mus=%d\n",file_head.nk);
    fprintf(stream,"moms=%d\n",file_head.nmoms);
    
    fprintf(stream,"beta=%f\n",file_head.beta);
    fprintf(stream,"ksea=%f\n",file_head.ksea);
    fprintf(stream,"musea=%f\n",file_head.musea);
    fprintf(stream,"csw=%f\n",file_head.csw);
   
    fprintf(stream,"masses=");
    for(i=file_head.nk;i<2*file_head.nk;++i)
            fprintf(stream,"%f\t",file_head.k[i]);
    fprintf(stream,"\n");
    
    fprintf(stream,"momenta=");
    for(i=0;i<file_head.nmoms;++i)
           fprintf(stream,"%f  %f   %f   %f\n",file_head.mom[i][0],file_head.mom[i][1],file_head.mom[i][2],file_head.mom[i][3]);
}

static void  read_file_head_bin(FILE *stream)
{
    int i;
    int nk_old,nmoms_old;
    if(file_head.allocated==1){
        nk_old=file_head.nk;
        nmoms_old=file_head.nmoms;
    }
    
    fread(&file_head.twist,sizeof(int),1,stream);
    fread(&file_head.nf,sizeof(int),1,stream);
    fread(&file_head.nsrc,sizeof(int),1,stream);
    fread(&file_head.l0,sizeof(int),1,stream);
    fread(&file_head.l1,sizeof(int),1,stream);
    fread(&file_head.l2,sizeof(int),1,stream);
    fread(&file_head.l3,sizeof(int),1,stream);
    fread(&file_head.nk,sizeof(int),1,stream);
    fread(&file_head.nmoms,sizeof(int),1,stream);
    if(file_head.allocated==1)
        error(nk_old!=file_head.nk || nmoms_old!=file_head.nmoms,1,"read_file_head_jack", " file head nk or nmoms has changed %d %d -> %d %d",nk_old,nmoms_old,file_head.nk,file_head.nmoms );

    fread(&file_head.beta,sizeof(double),1,stream);
    fread(&file_head.ksea,sizeof(double),1,stream);
    fread(&file_head.musea,sizeof(double),1,stream);
    fread(&file_head.csw,sizeof(double),1,stream);
   
    if(file_head.allocated==0){
        file_head.k=(double*)  malloc(sizeof(double)*2*file_head.nk);
        file_head.mom=(double**) malloc(sizeof(double*)*file_head.nmoms);
        for(i=0;i<file_head.nmoms;i++) 
            file_head.mom[i]=(double*) malloc(sizeof(double)*4);
        file_head.allocated=1;
    }
    for(i=0;i<2*file_head.nk;++i)
    	fread(&file_head.k[i],sizeof(double),1,stream);
    
    
    for(i=0;i<file_head.nmoms;i++) {
        fread(&file_head.mom[i][0],sizeof(double),1,stream);
        fread(&file_head.mom[i][1],sizeof(double),1,stream);
        fread(&file_head.mom[i][2],sizeof(double),1,stream);
        fread(&file_head.mom[i][3],sizeof(double),1,stream);

    }
}
static void  write_file_head(FILE *stream)
{
    int i;    
    
    fwrite(&file_head.twist,sizeof(int),1,stream);
    fwrite(&file_head.nf,sizeof(int),1,stream);
    fwrite(&file_head.nsrc,sizeof(int),1,stream);
    fwrite(&file_head.l0,sizeof(int),1,stream);
    fwrite(&file_head.l1,sizeof(int),1,stream);
    fwrite(&file_head.l2,sizeof(int),1,stream);
    fwrite(&file_head.l3,sizeof(int),1,stream);
    fwrite(&file_head.nk,sizeof(int),1,stream);
    fwrite(&file_head.nmoms,sizeof(int),1,stream);
    
    fwrite(&file_head.beta,sizeof(double),1,stream);
    fwrite(&file_head.ksea,sizeof(double),1,stream);
    fwrite(&file_head.musea,sizeof(double),1,stream);
    fwrite(&file_head.csw,sizeof(double),1,stream);
   
    fwrite(file_head.k,sizeof(double),2*file_head.nk,stream);

    for(i=0;i<file_head.nmoms;i++)  
        fwrite(file_head.mom[i],sizeof(double),4,stream);
}

void read_nconfs(int *s, int *c, FILE *stream){

   long int tmp;

   fread(s,sizeof(int),1,stream);
   
   fseek(stream, 0, SEEK_END);
   tmp = ftell(stream);
   tmp-= sizeof(double)* (file_head.nmoms*4 + file_head.nk*2+4 )+ sizeof(int)*10 ;
   //tmp-= sizeof(int)*2;
   (*c)= tmp/ (sizeof(int)+ (*s)*sizeof(double) );
 
  rewind(stream);
  read_file_head_bin(stream);
  fread(s,sizeof(int),1,stream);

}


double *constant_fit(int M, double in){
    double *r;
    
    r=(double*) malloc(sizeof(double)*M);
    r[0]=1.;
    
    return r;
}


int index_n_minus_r( int r)
{
    int ir;
    if (r==0) ir=1;
    if (r==1) ir=0;
    return ir;
}



void read_chunk(FILE *stream,int tmp,int index, double *obs, double ***to_write,int *vol, int si){
       int t;  
       fseek(stream,tmp+ index, SEEK_SET);
       fread(obs,sizeof(double),2*file_head.l0,stream); 
   
       for(t=0;t<file_head.l0;t++){
           (*to_write)[t][0]+=obs[t*2];
           (*to_write)[t][1]+=obs[t*2+1];
       }
       *vol+=1;

}

void read_twopt(FILE *stream,int size, int iconf , double ***to_write,int si, int ii,int ik1, int ik2, int imom1, int imom2 ){
   
   long int tmp;
   double *obs;
   //int cik1, cik2;
   int cimom1,cimom2,cr1;
   int t,vol=0,index;
   double re,im;
   int    vol3=file_head.l1*file_head.l2*file_head.l3; 

   tmp= sizeof(double)* (file_head.nmoms*4 + file_head.nk*2+4 )+ sizeof(int)*(12) ;
   tmp+=sizeof(double)*iconf*size+sizeof(int)*iconf;

   obs=(double*) malloc(2*file_head.l0*sizeof(double)); 
   
   cimom1=index_n_minus_theta(imom1);
   cimom2=index_n_minus_theta(imom2);
   //cik1=index_n_minus_kappa(ik1);
   //cik2=index_n_minus_kappa(ik2);
   
   index=sizeof(double)*2*index_mesonic_twopt(si,ii,0,ik1,ik2,imom1,imom2);
   int aaa=ftell(stream);
   fseek(stream, tmp+index, SEEK_SET);

   fread(obs,sizeof(double),2*file_head.l0,stream); 

   for(t=0;t<file_head.l0;t++){
       (*to_write)[t][0]=obs[t*2];
       (*to_write)[t][1]=obs[t*2+1];
   }
   vol++;
   /*
   index=sizeof(double)*2*index_mesonic_twopt(si,ii,0,ik1,ik2,imom2,imom1);
   read_chunk(stream, tmp, index,  obs,  to_write, &vol,  si);
  
   index=sizeof(double)*2*index_mesonic_twopt(si,ii,0,ik2,ik1,imom1,imom2);
   read_chunk(stream, tmp, index,  obs,  to_write, &vol,  si);

   if (  cimom1>=0 &&  cimom2 >=0 ){ 
       index=sizeof(double)*2*index_mesonic_twopt(si,ii,0,ik1,ik2,cimom1,cimom2);
       read_chunk(stream, tmp, index,  obs,  to_write, &vol,  si);

       index=sizeof(double)*2*index_mesonic_twopt(si,ii,0,ik1,ik2,cimom2,cimom1);
       read_chunk(stream, tmp, index,  obs,  to_write, &vol,  si);

       index=sizeof(double)*2*index_mesonic_twopt(si,ii,0,ik2,ik1,cimom1,cimom2);
       read_chunk(stream, tmp, index,  obs,  to_write, &vol,  si);


   }
   */
   for(t=0;t<file_head.l0;t++){
  	   (*to_write)[t][0]/=( (double) vol );
	   (*to_write)[t][1]/=( (double) vol );
   }
   


   free(obs);

}

 /*
void setup_single_file_jack(char  *save_name,char **argv, const char  *name,int Njack){
     FILE *f;
     mysprintf(save_name,NAMESIZE,"%s/%s",argv[3],name);
     f=fopen(save_name,"w+");
     error(f==NULL,1,"setup_file_jack ",
         "Unable to open output jackknife file %s/%s",argv[3],name);
     write_file_head(f);
     fwrite(&Njack,sizeof(int),1,f);
     fclose(f);
}*/
void setup_single_file_jack(struct observable *obs,char **argv, const char  *name_jack,const char  *name_plateaux, const char  *name_out,int Njack){
     
     mysprintf((*obs).name_out,NAMESIZE,"%s/%s",argv[3],name_out);
     (*obs).f_out=     open_file((*obs).name_out,"r");  
     print_file_head((*obs).f_out);
    
     if ( strcmp(argv[1],"read_plateaux")==0 ){
        mysprintf((*obs).name_plateaux,NAMESIZE,"%s/%s",argv[3],name_plateaux);
        (*obs).f_plateaux=     open_file((*obs).name_plateaux,"r");        
     }
     
     
     
     if( strcmp(argv[4],"jack")==0)
        mysprintf((*obs).name_jack,NAMESIZE,"%s/%s_jack",argv[3],name_jack);
     else if( strcmp(argv[4],"boot")==0)
        mysprintf((*obs).name_jack,NAMESIZE,"%s/%s_boot",argv[3],name_jack);
     else 
        error(0==0,0,"setup_file_jack","argv[4] is neither jack or boot");
     
     (*obs).f_jack=open_file((*obs).name_jack,"w+");
     write_file_head((*obs).f_jack);
     fwrite(&Njack,sizeof(int),1,(*obs).f_jack);
     fclose((*obs).f_jack);
}
void close_obs(struct observable *obs){
    fclose((*obs).f_out);
    fclose((*obs).f_plateaux);
    //fclose((*obs).f_jack);
}

/*
void setup_file_jack(char **argv,int Njack,struct  database_file_barion_QED_jack  *file_jack_barion_QED){
    if( strcmp(argv[4],"jack")==0){
        setup_single_file_jack((*file_jack_barion_QED).M_PS,argv,"jackknife/M_{PS}_jack",Njack);
        setup_single_file_jack((*file_jack_barion_QED).Zf_PS,argv,"jackknife/Zf_{PS}_jack",Njack);
        setup_single_file_jack((*file_jack_barion_QED).m_PCAC,argv,"jackknife/Zf_{PS}_jack",Njack);
    }
    else if( strcmp(argv[4],"boot")==0){
        setup_single_file_jack((*file_jack_barion_QED).M_PS,argv,"jackknife/M_{PS}_boot",Njack);
        setup_single_file_jack((*file_jack_barion_QED).Zf_PS,argv,"jackknife/Zf_{PS}_boot",Njack);
        setup_single_file_jack((*file_jack_barion_QED).m_PCAC,argv,"jackknife/Zf_{PS}_boot",Njack);    
    }
    else 
        error(0==0,0,"setup_file_jack","argv[4] is neither jack or boot");

  

}
*/

int main(int argc, char **argv){
   int size_meson_2pt, sizeAmuP,sizeAmuGP,sizeVV,sizeVmuGP;
   int i,j,t,iG,iG0;
   clock_t t1,t2;
   double dt;
   int *sym;

   int *iconf,confs;
   double ****data,****data_bin,**tmp; 
   char c;
   double *in;

   //double ****M,****vec;
   //****projected_O;
   //double  ****lambda,****lambda0;
   
   double *fit,***y,*x,*m,*me;
   

   double ****conf_jack,**r,**met;
   int Ncorr=1;
   int t0=2;
   
   FILE  *meson_2pt=NULL;
   
   FILE *plateaux_m_sl=NULL; 
   
   
   char bin_name[NAMESIZE];
   
   struct observable obs_M_PS, obs_Zf_PS, obs_m_pcac;
   
   error(argc!=6,1,"main ",
         "usage argc!=5: ./form_factors blind/see/read_plateaux -p inpath   jack/boot  pdf/only_fit_R/no");

   error(strcmp(argv[2],"-p")!=0,1,"main ",
         "missing -p \n usage: ./form_factors blind/see/read_plateaux -p inpath   jack/boot  pdf/only_fit_R/no");

    error(strcmp(argv[4],"jack")!=0 && strcmp(argv[4],"boot")!=0 ,2,"main ",
         "choose jack or boot \n usage: ./form_factors blind/see/read_plateaux -p inpath   jack/boot   pdf/only_fit_R/no");
    
    
   char namefile[NAMESIZE];
   
   
  
        
      
 
  
  
  mysprintf(bin_name,NAMESIZE,"_bin.dat");

   mysprintf(namefile,NAMESIZE,"%s/data/meson_2pt%s",argv[3],bin_name);
   meson_2pt=open_file(namefile,"r");
   
   
   read_file_head_bin(meson_2pt);
   read_nconfs(&size_meson_2pt,&confs,meson_2pt);   
   printf("meson_2pt\t size=%d  confs=%d  \n",size_meson_2pt,confs);
 
   
   
   
   
   
   int bin=1;
   int Neff=confs/bin;
   int Njack=Neff+1;
   if( strcmp(argv[4],"jack")==0)
                Njack=Neff+1;
   if( strcmp(argv[4],"boot")==0)
                Njack=Nbootstrap+1;
   
   
   setup_single_file_jack(&obs_M_PS,argv,"jackknife/M_{PS}",   "plateaux_m_sl.txt",   "out/out_E0.txt",   Njack);

   
   
   struct  database_file_barion_QED_jack  file_jack_barion_QED;
   //setup_file_jack(argv,Njack, &file_jack_barion_QED);
   
    
   iconf=(int*) malloc(sizeof(int)*confs);
   
   
   int var=2,si;
   
       
   
   char name[NAMESIZE];
   int index;
   
   int vol=0;
   

   
   
   double **mass_jack_fit=      (double**) malloc(sizeof(double*)*size_meson_2pt/file_head.l0);
   double **Zf_PS_jack_fit= (double**) malloc(sizeof(double*)*size_meson_2pt/file_head.l0);
   double **m_PCAC_fit= (double**) malloc(sizeof(double*)*size_meson_2pt/file_head.l0);
   
   
   
   double a ,b;
   
   /*fread(&i,sizeof(int),1,meson_2pt);
   printf("%d\n",i);
   fread(&i,sizeof(int),1,meson_2pt);
   printf("%d\n",i);
   fread(&a,sizeof(double),1,meson_2pt);
   printf("%g\n",a);
   fread(&a,sizeof(double),1,meson_2pt);
   printf("%g\n",a);
   fread(&a,sizeof(double),1,meson_2pt);
   printf("%g\n",a);
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
///////////////////meson 2pt     
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
     
  data=       calloc_corr(confs, var,  file_head.l0 );
  sym=(int*) malloc(sizeof(int)*var);
  int ikt,iks,imom0,imoms;   
  int i_m;
  
  for(iks=0;iks<file_head.nk;iks++){
  for(ikt=0;ikt<file_head.nk;ikt++){     
  for(imoms=0;imoms<file_head.nmoms;imoms++){
  for(imom0=0;imom0<file_head.nmoms;imom0++){       
       
     a=timestamp();  
     for (i=0;i<confs;i++){
               read_twopt(meson_2pt,size_meson_2pt,i ,&data[i][0],12,0,ikt,iks,imom0,imoms ); 
               read_twopt(meson_2pt,size_meson_2pt,i ,&data[i][1],12,1,ikt,iks,imom0,imoms );  

     }       
      
     b=timestamp();
     printf("time=%f\n",b-a);
        
     symmetrise_corr(confs, 0, file_head.l0,data);

     data_bin=binning(confs, var, file_head.l0 ,data, bin);
     conf_jack=create_jack(Neff, var, file_head.l0, data_bin);

       
     i=index_n_twopt_fit(ikt,iks,imom0,imoms);       
     i_m=index_n_twopt_fit(ikt,iks,0,0);      
     get_kinematic( ikt,  iks,imom0,  imoms );
       
       mass_jack_fit[i]=compute_effective_mass1(  argv, kinematic_2pt, (char*) "meson_2pt", conf_jack,  Njack  ,0,&obs_M_PS);

       
       free_corr(Neff, var, file_head.l0 ,data_bin);
       free_jack(Njack,var , file_head.l0, conf_jack);
       
   }}}} //end loop  ikt iks imom0  imoms
   
   free_corr(confs, var,  file_head.l0 ,data);
   
   close_obs(&obs_M_PS);
   //fclose(out_E0);     fclose(out_Zf_PS);      fclose(out_m_PCAC);    

  
  for(iks=0;iks<file_head.nk;iks++){
  for(ikt=0;ikt<file_head.nk;ikt++){     
  for(imoms=0;imoms<file_head.nmoms;imoms++){
  for(imom0=0;imom0<file_head.nmoms;imom0++){  
      i=index_n_twopt_fit(ikt,iks,imom0,imoms);       
        
      free(mass_jack_fit[i]);
        
      
      
      
 
      
  }}}}
  
  free(mass_jack_fit);
  free(Zf_PS_jack_fit);
  free(m_PCAC_fit);
  free(sym); free(iconf);
   

  return 0;   
}
