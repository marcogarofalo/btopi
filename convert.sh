#!/bin/bash

date
obs=threept
#obs=twopt


m_list=(0.0042  0.690  )
th_list=(0.00  -0.5557 0.5557  )  #L16
#-0.5557 0.5557   

file_head_twist=1
file_head_nf=0
file_head_nsrc=0
file_head_l0=64
file_head_l1=32
file_head_l2=32
file_head_l3=32
file_head_nk=$((${#m_list[@]} *2))
#?? pure negativi per fare r=-1
file_head_nmoms=${#th_list[@]}

file_head_beta=6.105
file_head_ksea=0.135443
file_head_musea=0.0
file_head_csw=1.464939

sanfo_out="../out"
contractions=(V0P5 V1P5 V2P5 V3P5 P5P5) #follow the order
#contractions=(P5P5) #follow the order

if [ $obs = "twopt" ]
then
size=$((file_head_nk*file_head_nk*file_head_nmoms*file_head_nmoms*1*file_head_l0*2))
fi
if [ $obs = "threept" ]
then
size=$((file_head_nk*file_head_nk*file_head_nk*file_head_nmoms*file_head_nmoms*${#contractions[@]}*file_head_l0*2))
fi


write_header_ASCI ()
{
echo $file_head_twist >  to_read
echo $file_head_nf    >> to_read
echo $file_head_nsrc  >> to_read
echo $file_head_l0    >> to_read
echo $file_head_l1    >> to_read
echo $file_head_l2    >> to_read
echo $file_head_l3    >> to_read
echo $file_head_nk    >> to_read
echo $file_head_nmoms >> to_read

echo $file_head_beta  >> to_read
echo $file_head_ksea  >> to_read
echo $file_head_musea >> to_read
echo $file_head_csw   >> to_read



for((ik=0;ik<$((${#m_list[@]}));ik++))
do
    k=${m_list[${ik}]}  
    echo $file_head_ksea >> to_read
    echo $file_head_ksea >> to_read
done
for((ik=0;ik<$((${#m_list[@]}));ik++))
do
    k=${m_list[${ik}]}  
    echo $k >> to_read
    echo -$k >> to_read
done


for((ith=0;ith<$((${file_head_nmoms}));ith++))
do
    th=${th_list[${ith}]}  
    mom=`echo $th/2. | bc -l | awk '{printf "%.5f", $1}'`
    echo  0.5  $mom  $mom  $mom  >> to_read
done
echo  $size  >>to_read
}





#confs=`ls $sanfo_out | grep -v 0900 | grep -v 02900 `
#confs=`ls $sanfo_out`
#confs="01800 02200 02300 02500 02600 02700 02800 02900 03000 03100 03300 03400 03500 03600 03900 04000 04200 04300 04400 04600 05400 05600 05700 05800 07000 07200 07300"
#confs="00900  01100  01300  01500  01700  01900  02100  02300  02500  02700  02900  03100  03300  03500  03700  03900  04100  04300  04500  04700  04900  05100 01000  01200  01400  01600  02000  02200  02400  02600  02800  03000  03200  03400  03600  03800  04000  04200  04400  04600  04800  05000  05200"

confs=`du -h -s $sanfo_out/* | grep -E  '11M' | awk '{print $2}'  `
confs=${confs//"$sanfo_out/"}

echo  the configuration found are
echo $confs
#echo total number of configuration  `ls $sanfo_out | grep -v 0900 | grep -v 02900 | wc -l `
#echo total number of configuration  `ls $sanfo_out |  wc -l `
echo ""

gcc convert.c -o convert

if [ $obs = "threept" ]
then
write_header_ASCI

for conf in $confs
do  
    printf '%s\t'   $conf  
    echo $conf  >> to_read
    sed '/^$/d' $sanfo_out/$conf/mes_contr_3pts_ss | grep -v Contraction > tmp   #remove empty lines
    for((ic=0;ic<${#contractions[@]};ic++))
    do
        contr=${contractions[ic]}
        cat tmp | grep -v $contr  > tmp1
        mv  tmp1  tmp
    done
    cat tmp >> to_read
done
date
echo converting in to binary
./convert
mv to_read_bin.dat   meas_3pt.dat
rm tmp to_read
#sbatch scr.sh

fi
   
if [ $obs = "twopt" ]
then
date
for((ic=0;ic<${#contractions[@]};ic++))
do
   echo ${contractions[ic]} 
   write_header_ASCI
   for conf in $confs
   do  
       printf '%s\t'   $conf  
       echo $conf  >> to_read
       sed '/^$/d' $sanfo_out/$conf/mes_contr_2pts_ss  > tmp   #remove empty lines
       cat tmp | grep -A$file_head_l0 ${contractions[ic]}  | sed '/--/d' | grep -v  ${contractions[ic]} > tmp1
       cat tmp1 >> to_read
       
   done
   date
   echo `wc -l to_read`
   ./convert
   mv to_read_bin.dat   meas_2pt_${contractions[ic]}.dat
done
rm tmp tmp1 to_read

fi

date  
